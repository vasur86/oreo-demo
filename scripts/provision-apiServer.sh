#!/bin/sh

rm -rf $PWD/apiServer
mkdir $PWD/apiServer

cd $PWD/apiServer
if [ "$?" != "0" ]; then
	exit 1
fi


wget https://raw.githubusercontent.com/vasur86/oreo-demo/master/docker-files/apiServer-Dockerfile
if [ "$?" != "0" ]; then
	exit 1
fi

cp apiServer-Dockerfile Dockerfile
if [ "$?" != "0" ]; then
	exit 1
fi

echo $PWD
#cp /media/vm_shared/project/code/realestate.zip $PWD/realestate.zip

docker build -t "apiserver:latest" .
if [ "$?" != "0" ]; then
	exit 1
fi

docker stop apiserver
docker rm apiserver

docker run -d -p 5000:5000 --name apiserver -v /media/sf_vm_shared/project/code:/code apiserver:latest python /code/realestate/api.py
if [ "$?" != "0" ]; then
	exit 1
fi

cd ..
rm -rf apiServer

exit 0