#!/bin/sh

service docker stop

echo "`ifconfig eth0 | grep "inet addr" | awk -F '[: ]+' '{print $4}'` mongoHost" >> /etc/hosts
echo mongoHost > /etc/hostname

apt-get update
if [ "$?" != "0" ]; then
	exit 1
fi


apt-get install apt-transport-https ca-certificates
if [ "$?" != "0" ]; then
	exit 1
fi


apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
if [ "$?" != "0" ]; then
	exit 1
fi


echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
if [ "$?" != "0" ]; then
	exit 1
fi


apt-get update
if [ "$?" != "0" ]; then
	exit 1
fi


apt-get purge lxc-docker
if [ "$?" != "0" ]; then
	exit 1
fi


apt-cache policy docker-engine
if [ "$?" != "0" ]; then
	exit 1
fi


apt-get update
if [ "$?" != "0" ]; then
	exit 1
fi


apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual
if [ "$?" != "0" ]; then
	exit 1
fi


apt-get update
if [ "$?" != "0" ]; then
	exit 1
fi


apt-get install -y docker-engine
if [ "$?" != "0" ]; then
	exit 1
fi


service docker start
if [ "$?" != "0" ]; then
	exit 1
fi


docker run hello-world
if [ "$?" != "0" ]; then
	exit 1
fi
