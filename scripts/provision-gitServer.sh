#!/bin/sh

rm -rf $PWD/gitServer
mkdir $PWD/gitServer

cd $PWD/gitServer
if [ "$?" != "0" ]; then
	exit 1
fi


wget https://raw.githubusercontent.com/vasur86/oreo-demo/master/docker-files/gitServer-Dockerfile
if [ "$?" != "0" ]; then
	exit 1
fi

cp gitServer-Dockerfile Dockerfile  
if [ "$?" != "0" ]; then
	exit 1
fi

echo $PWD
#cp /media/vm_shared/project/code/realestate.zip $PWD/realestate.zip

docker build -t "gitserver:latest" .
if [ "$?" != "0" ]; then
	exit 1
fi

docker stop gitserver
docker rm gitserver

docker run -d -p 5001:3000 --name gitserver -v /media/vm_shared/project/backup:/backup gitserver:latest
if [ "$?" != "0" ]; then
	exit 1
fi

cd ..
rm -rf gitServer

exit 0