#!/bin/sh

rm -rf $PWD/mongoServer
mkdir $PWD/mongoServer

cd $PWD/mongoServer
if [ "$?" != "0" ]; then
	exit 1
fi


wget https://raw.githubusercontent.com/vasur86/oreo-demo/master/docker-files/mongoServer-Dockerfile
if [ "$?" != "0" ]; then
	exit 1
fi

cp mongoServer-Dockerfile Dockerfile
if [ "$?" != "0" ]; then
	exit 1
fi

echo $PWD
#cp /media/vm_shared/project/code/realestate.zip $PWD/realestate.zip

docker build -t "mongoserver:latest" .
if [ "$?" != "0" ]; then
	exit 1
fi

docker stop mongoserver
docker rm mongoserver

docker run -d -p 27017:27017 --name mongoserver -v /media/sf_vm_shared/project/backup:/backup mongoserver:latest
if [ "$?" != "0" ]; then
	exit 1
fi

cd ..
rm -rf mongoServer


apt-get install -y mongodb-clients
if [ "$?" != "0" ]; then
	exit 1
fi


mongorestore -h dockerhost --port 27017 /media/sf_vm_shared/project/backup/db/mongo-api
if [ "$?" != "0" ]; then
	exit 1
fi

exit 0